# GitLab Curly Braces Bug

Found this when creating a [cookiecutter](https://github.com/audreyr/cookiecutter) template.
If you try to browse to the `{{ cookiecutter.project_slug }}` dir from the GitLab web interface,
it will not enter the directory correctly. You'll get a recursion of `{{ cookiecutter.project_slug }}`s in your path.
